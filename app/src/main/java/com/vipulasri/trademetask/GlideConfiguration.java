package com.vipulasri.trademetask;

import android.app.ActivityManager;
import android.content.Context;
import android.support.v4.app.ActivityManagerCompat;
import android.util.Log;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

@GlideModule
public final class GlideConfiguration extends AppGlideModule {

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {

        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        if (activityManager != null) {
            builder.setDefaultRequestOptions(new RequestOptions()
                    .format(ActivityManagerCompat.isLowRamDevice(activityManager)? DecodeFormat.PREFER_RGB_565 : DecodeFormat.PREFER_ARGB_8888)
                    .disallowHardwareConfig())
                    .setLogLevel(Log.DEBUG);
        }

        super.applyOptions(context, builder);
    }
}

