
package com.vipulasri.trademetask.dto;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.vipulasri.trademetask.model.Category;

import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CategoryResponse {

    @SerializedName("Subcategories")
    private List<Category> mSubcategories;

    public List<Category> getSubcategories() {
        return mSubcategories;
    }

    public void setSubcategories(List<Category> Subcategories) {
        mSubcategories = Subcategories;
    }

}
