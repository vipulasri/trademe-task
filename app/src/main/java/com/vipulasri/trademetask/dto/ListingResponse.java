
package com.vipulasri.trademetask.dto;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.vipulasri.trademetask.model.Listing;

import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ListingResponse {

    @SerializedName("List")
    private List<Listing> mList;
    @SerializedName("Page")
    private Long mPage;
    @SerializedName("PageSize")
    private Long mPageSize;
    @SerializedName("TotalCount")
    private Long mTotalCount;

    public List<Listing> getList() {
        return mList;
    }

    public void setList(List<Listing> List) {
        mList = List;
    }

    public Long getPage() {
        return mPage;
    }

    public void setPage(Long Page) {
        mPage = Page;
    }

    public Long getPageSize() {
        return mPageSize;
    }

    public void setPageSize(Long PageSize) {
        mPageSize = PageSize;
    }

    public Long getTotalCount() {
        return mTotalCount;
    }

    public void setTotalCount(Long TotalCount) {
        mTotalCount = TotalCount;
    }

}
