
package com.vipulasri.trademetask.ui.base;

public interface BasePresenter<T> {

    void takeView(T view);
    void dropView();

}
