package com.vipulasri.trademetask.ui.base

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.extensions.whenNotNull
import dagger.android.support.DaggerAppCompatActivity

@SuppressLint("Registered")
open class BaseActivity : DaggerAppCompatActivity(), BaseErrorInterface {

    var toolbar: Toolbar? = null

    private lateinit var progressDialog: MaterialDialog

    //If back button is displayed in action bar, return false
    protected var isDisplayHomeAsUpEnabled: Boolean
        get() = false
        set(value) {
            if (supportActionBar != null)
                supportActionBar!!.setDisplayHomeAsUpEnabled(value)
        }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)

        injectViews()

        //Displaying the back button in the action bar
        if (isDisplayHomeAsUpEnabled) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    protected fun injectViews() {
        toolbar = findViewById(R.id.toolbar)
        setupToolbar()
        initMaterialDialog()
    }

    fun setContentViewWithoutInject(layoutResId: Int) {
        super.setContentView(layoutResId)
    }

    protected fun setupToolbar() {
        whenNotNull(toolbar) {
            setSupportActionBar(it)
        }
    }

    fun setActivityTitle(title: Int) {
        whenNotNull(supportActionBar) {
            it.setTitle(title)
        }
    }

    fun setActivityTitle(title: String) {
        whenNotNull(supportActionBar) {
            it.setTitle(title)
        }
    }

    fun getToolbarTitleTextView(): View {
        val childCount = toolbar!!.childCount
        for (i in 0 until childCount) {
            val child = toolbar!!.getChildAt(i)
            if (child is TextView) {
                return child
            }
        }

        return View(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //Menu
        when (item.itemId) {
        //When home is clicked
            android.R.id.home -> {
                onActionBarHomeIconClicked()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun setHomeAsUpIndicator(drawable: Drawable) {
        if (supportActionBar != null)
            supportActionBar!!.setHomeAsUpIndicator(drawable)
    }

    //Method for when home button is clicked
    open fun onActionBarHomeIconClicked() {
        if (isDisplayHomeAsUpEnabled) {
            onBackPressed()
        } else {
            finish()
        }
    }

    private fun showProgressDialog() {
        if (!isFinishing) {
            progressDialog.show()
        }
    }

    private fun initMaterialDialog() {
        progressDialog = MaterialDialog.Builder(this)
                .content(R.string.please_wait)
                .cancelable(false)
                .progress(true, 0)
                .build()
    }

    private fun hideProgressDialog() {
        progressDialog.dismiss()
    }

    override fun onPause() {
        super.onPause()
        hideProgressDialog()
    }

    fun showLoading(active: Boolean) {
        if(active) showProgressDialog() else hideProgressDialog()
    }

    fun showError(throwable: Throwable) {
        showError(throwable, findViewById(android.R.id.content))
    }
}
