package com.vipulasri.trademetask.ui.home

import android.util.Log
import com.vipulasri.trademetask.data.CategoryRepository
import com.vipulasri.trademetask.extensions.whenNotNull
import com.vipulasri.trademetask.inject.ActivityScoped
import com.vipulasri.trademetask.inject.FragmentScoped
import javax.inject.Inject

import io.reactivex.disposables.CompositeDisposable

@ActivityScoped
class HomePresenter @Inject
internal constructor(private val mCategoryRepository: CategoryRepository) : HomeContract.Presenter {

    private var mView: HomeContract.View? = null

    private val mDisposables: CompositeDisposable = CompositeDisposable()

    override fun search(query: String) {
        whenNotNull(mView) {
            it.showLoading(true)
        }

        val disposable = mCategoryRepository
                .search(query)
                .subscribe({ categories ->
                    mView?.showLoading(false)
                    mView?.showSearchResults(categories)
                }, { throwable ->
                    Log.e("error", "->" + throwable.message)
                    mView?.showLoading(false)
                    mView?.showError(throwable)
                })

        mDisposables.add(disposable)
    }

    override fun takeView(view: HomeContract.View) {
        mView = view
    }

    override fun dropView() {
        mView = null
        mDisposables.clear()
    }

    companion object {

        private val TAG = HomeContract::class.java.simpleName
    }
}
