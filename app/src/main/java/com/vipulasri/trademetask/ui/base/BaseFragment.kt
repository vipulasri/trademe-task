package com.vipulasri.trademetask.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.extensions.whenNotNull
import dagger.android.support.DaggerFragment

open class BaseFragment : DaggerFragment(), BaseErrorInterface {

    private lateinit var progressDialog: MaterialDialog

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initMaterialDialog()
    }

    private fun showProgressDialog() {
        if (!isDetached) {
            progressDialog.show()
        }
    }

    private fun initMaterialDialog() {
        progressDialog = MaterialDialog.Builder(context!!)
                .content(R.string.please_wait)
                .cancelable(false)
                .progress(true, 0)
                .build()
    }

    private fun hideProgressDialog() {
        progressDialog.dismiss()
    }

    override fun onPause() {
        super.onPause()
        hideProgressDialog()
    }

    fun showLoading(active: Boolean) {
        if(active) showProgressDialog() else hideProgressDialog()
    }

    fun showError(throwable: Throwable) {
        showError(throwable, view!!)
    }
}
