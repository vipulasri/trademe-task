
package com.vipulasri.trademetask.ui.base;

public interface BaseView<T> {

    void showLoading(boolean active);
    void showError(Throwable throwable);

}
