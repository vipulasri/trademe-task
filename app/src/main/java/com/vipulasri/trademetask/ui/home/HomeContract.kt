package com.vipulasri.trademetask.ui.home

import com.vipulasri.trademetask.model.Listing
import com.vipulasri.trademetask.ui.base.BasePresenter
import com.vipulasri.trademetask.ui.base.BaseView

interface HomeContract {

    interface View : BaseView<Presenter> {

        fun showSearchResults(listings: List<Listing>)

    }

    interface Presenter : BasePresenter<View> {

        fun search(query: String)

        override fun takeView(view: HomeContract.View)

        override fun dropView()
    }
}
