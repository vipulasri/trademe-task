package com.vipulasri.trademetask.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.model.Listing

class SearchAdapter(private val mContext: Context, resource: Int, val mItems: List<Listing>) : ArrayAdapter<Listing>(mContext, resource) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent, "getView")
    }

    private fun initView(position: Int, convertView: View?, parent: ViewGroup, whichMethod: String): View {
        val inflater = LayoutInflater.from(parent.context)
        var view = convertView
        val holder: ViewHolder

        if (convertView == null) {

            view = inflater.inflate(R.layout.layout_spinner_multi_row, parent, false)

            holder = ViewHolder()
            holder.name = view.findViewById<TextView>(android.R.id.text1)
            holder.city = view.findViewById<TextView>(android.R.id.text2)
            view.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }

        val listing = mItems[position]

        holder.name!!.text = listing.Title
        holder.city!!.text = listing.Region

        return view!!
    }

    private class ViewHolder {
        internal var name: TextView? = null
        internal var city: TextView? = null
    }

    override fun getCount(): Int {
        return mItems.size
    }

    override fun getItem(position: Int): Listing? {
        return mItems[position]
    }
}