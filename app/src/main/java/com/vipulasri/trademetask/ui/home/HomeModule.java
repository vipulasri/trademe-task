package com.vipulasri.trademetask.ui.home;

import com.vipulasri.trademetask.inject.ActivityScoped;
import com.vipulasri.trademetask.inject.FragmentScoped;
import com.vipulasri.trademetask.ui.categories.CategoriesContract;
import com.vipulasri.trademetask.ui.categories.CategoriesFragment;
import com.vipulasri.trademetask.ui.categories.CategoriesPresenter;
import com.vipulasri.trademetask.ui.listing.ListingsContract;
import com.vipulasri.trademetask.ui.listing.ListingsFragment;
import com.vipulasri.trademetask.ui.listing.ListingsPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HomeModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract CategoriesFragment categoriesFragment();

    @FragmentScoped
    @Binds
    abstract CategoriesContract.Presenter categoriesPresenter(CategoriesPresenter presenter);

    @FragmentScoped
    @ContributesAndroidInjector
    abstract ListingsFragment listingsFragment();

    @FragmentScoped
    @Binds
    abstract ListingsContract.Presenter listingsPresenter(ListingsPresenter presenter);

    @ActivityScoped
    @Binds
    abstract HomeContract.Presenter homePresenter(HomePresenter presenter);
}
