package com.vipulasri.trademetask.ui.listingDetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.text.TextUtils
import android.util.Log
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.extensions.loadImageUrl
import com.vipulasri.trademetask.extensions.then
import com.vipulasri.trademetask.extensions.whenNotNull
import com.vipulasri.trademetask.model.Listing
import com.vipulasri.trademetask.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_listing_details.*
import kotlinx.android.synthetic.main.layout_listing_details.*

class ListingDetailsActivity : BaseActivity() {

    val EXTRA_LISTING = "EXTRA_LISTING"

    private lateinit var mListing: Listing

    fun launchActivity(startingActivity: Context, listing: Listing) {
        val intent = Intent(startingActivity, ListingDetailsActivity::class.java)
        intent.putExtra(EXTRA_LISTING, listing)
        startingActivity.startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listing_details)
        isDisplayHomeAsUpEnabled = true

        mListing = intent.getParcelableExtra(EXTRA_LISTING)
        Log.d("category", "->"+mListing.Title)

        setActivityTitle(mListing.Title)

        getToolbarTitleTextView().alpha = 0f
        val listener = AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (collapsingToolbar.height + verticalOffset < collapsingToolbar.scrimVisibleHeightTrigger) {
                getToolbarTitleTextView().animate().alpha(1f).duration = 200
            } else {
                getToolbarTitleTextView().animate().alpha(0f).duration = 200
            }
        }

        appBarLayout.addOnOffsetChangedListener(listener)

        whenNotNull(mListing.PictureHref) {
            image_listing.loadImageUrl(it)
        }

        text_listing_id.text = TextUtils.concat(getString(R.string.hash), mListing.ListingId.toString())
        text_listing_name.text = mListing.Title
        text_listing_region.text = mListing.Region
        text_listing_suburb.text = mListing.Suburb

        text_listing_body.text = getString(R.string.no_description_available)
        if(mListing.Body.orEmpty().isNotEmpty()) {
            text_listing_body.text = mListing.Body
        }
    }
}
