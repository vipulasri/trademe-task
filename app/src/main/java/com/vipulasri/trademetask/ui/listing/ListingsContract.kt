package com.vipulasri.trademetask.ui.listing

import com.vipulasri.trademetask.model.Listing
import com.vipulasri.trademetask.ui.base.BasePresenter
import com.vipulasri.trademetask.ui.base.BaseView

interface ListingsContract {

    interface View : BaseView<Presenter> {

        fun showListings(listings: List<Listing>)

    }

    interface Presenter : BasePresenter<View> {

        fun getListingByCategory(category: String)

        override fun takeView(view: ListingsContract.View)

        override fun dropView()
    }
}
