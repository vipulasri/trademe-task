package com.vipulasri.trademetask.ui.categories

import com.vipulasri.trademetask.model.Category
import com.vipulasri.trademetask.ui.base.BasePresenter
import com.vipulasri.trademetask.ui.base.BaseView

interface CategoriesContract {

    interface View : BaseView<Presenter> {

        fun showCategories(categories: List<Category>)

    }

    interface Presenter : BasePresenter<View> {

        fun getTopCategories()

        override fun takeView(view: CategoriesContract.View)

        override fun dropView()
    }
}
