package com.vipulasri.trademetask.ui.listing

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.model.Category
import com.vipulasri.trademetask.ui.base.BaseActivity

class ListingsActivity : BaseActivity() {

    val EXTRA_CATEGORY = "EXTRA_CATEGORY"

    private lateinit var mCategory: Category

    fun launchActivity(startingActivity: Context, category: Category) {
        val intent = Intent(startingActivity, ListingsActivity::class.java)
        intent.putExtra(EXTRA_CATEGORY, category)
        startingActivity.startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listings)
        isDisplayHomeAsUpEnabled = true

        mCategory = intent.getParcelableExtra(EXTRA_CATEGORY)
        Log.d("category", "->"+mCategory.Number)

        setActivityTitle(mCategory.Name + " " + getString(R.string.listing))

        showListingFragment(mCategory)
    }

    private fun showListingFragment(category: Category) {
        replaceFragment(ListingsFragment().newInstance(category))
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.categoryListingFragment, fragment)
                .commit()
    }

}