package com.vipulasri.trademetask.ui.listing

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.extensions.inflateView
import com.vipulasri.trademetask.extensions.loadImageUrl
import com.vipulasri.trademetask.extensions.whenNotNull
import com.vipulasri.trademetask.model.Listing
import kotlinx.android.synthetic.main.item_listing.view.*

class ListingsAdapter(var mFeedList: MutableList<Listing>) : RecyclerView.Adapter<ListingsAdapter.ActivityViewHolder>() {

    interface Callbacks {
        fun onListingClick(listing: Listing)
    }

    private var context: Context? = null
    private var callbacks: Callbacks? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivityViewHolder {
        context = parent.context
        return ActivityViewHolder(inflateView(R.layout.item_listing, parent, false))
    }

    override fun onBindViewHolder(holder: ActivityViewHolder, position: Int) {
        val listing = mFeedList[position]
        holder.name.text = listing.Title
        whenNotNull(listing.PictureHref) {
            holder.image.loadImageUrl(it)
        }

        holder.itemView.setOnClickListener { callbacks?.onListingClick(listing) }
    }

    override fun getItemCount(): Int {
        return mFeedList.size;
    }

    fun setCallbacks(callbacks: Callbacks) {
        this.callbacks = callbacks
    }

    class ActivityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.text_listing_name
        val image = itemView.image_listing
    }

}