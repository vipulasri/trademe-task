package com.vipulasri.trademetask.ui.listing

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.extensions.dpToPx
import com.vipulasri.trademetask.extensions.inflateView
import com.vipulasri.trademetask.inject.FragmentScoped
import com.vipulasri.trademetask.model.Category
import com.vipulasri.trademetask.model.Listing
import com.vipulasri.trademetask.ui.base.BaseFragment
import com.vipulasri.trademetask.ui.listingDetails.ListingDetailsActivity
import com.vipulasri.trademetask.widgets.BottomSpaceItemDecoration
import kotlinx.android.synthetic.main.fragment_listing.*
import javax.inject.Inject

@FragmentScoped
class ListingsFragment : BaseFragment(), ListingsContract.View, ListingsAdapter.Callbacks {

    @Inject
    lateinit var mPresenter: ListingsPresenter

    private lateinit var mCategory: Category
    private var mListings = arrayListOf<Listing>()
    private lateinit var mCategoryAdapter: ListingsAdapter

    fun newInstance(category: Category): ListingsFragment {
        val listingsFragment = ListingsFragment()
        val bundle = Bundle()
        bundle.putParcelable(ListingsActivity().EXTRA_CATEGORY, category)
        listingsFragment.arguments = bundle
        return listingsFragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflateView(R.layout.fragment_listing, container!!, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mCategory = arguments!!.getParcelable(ListingsActivity().EXTRA_CATEGORY)

        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.addItemDecoration(BottomSpaceItemDecoration(dpToPx(7f, context!!)))

        mCategoryAdapter = ListingsAdapter(mListings)
        mCategoryAdapter.setCallbacks(this)
        recyclerView.adapter = mCategoryAdapter
    }

    override fun onResume() {
        super.onResume()
        mPresenter.takeView(this)
        mPresenter.getListingByCategory(mCategory.Number)
    }

    override fun onPause() {
        super.onPause()
        mPresenter.dropView()
    }

    override fun showListings(listings: List<Listing>) {
        mListings.clear()
        mListings.addAll(listings)
        mCategoryAdapter.notifyDataSetChanged()
    }

    override fun onListingClick(listing: Listing) {
        ListingDetailsActivity().launchActivity(context!!, listing)
    }

}