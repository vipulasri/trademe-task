package com.vipulasri.trademetask.ui.categories

import android.util.Log
import com.vipulasri.trademetask.data.CategoryRepository
import com.vipulasri.trademetask.extensions.whenNotNull
import com.vipulasri.trademetask.inject.FragmentScoped
import javax.inject.Inject

import io.reactivex.disposables.CompositeDisposable

@FragmentScoped
class CategoriesPresenter @Inject
internal constructor(private val mCategoryRepository: CategoryRepository) : CategoriesContract.Presenter {

    private var mView: CategoriesContract.View? = null

    private val mDisposables: CompositeDisposable = CompositeDisposable()

    override fun getTopCategories() {

        whenNotNull(mView) {
            it.showLoading(true)
        }

        val disposable = mCategoryRepository
                .getTopCategories()
                .subscribe({ categories ->
                    mView?.showLoading(false)
                    mView?.showCategories(categories)
                }, { throwable ->
                    Log.e("error", "->" + throwable.message)
                    mView?.showLoading(false)
                    mView?.showError(throwable)
                })

        mDisposables.add(disposable)
    }

    override fun takeView(view: CategoriesContract.View) {
        mView = view
    }

    override fun dropView() {
        mView = null
        mDisposables.clear()
    }

    companion object {

        private val TAG = CategoriesContract::class.java.simpleName
    }
}
