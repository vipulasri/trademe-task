package com.vipulasri.trademetask.ui.categories

import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.extensions.dpToPx
import com.vipulasri.trademetask.extensions.inflateView
import com.vipulasri.trademetask.inject.FragmentScoped
import com.vipulasri.trademetask.model.Category
import com.vipulasri.trademetask.ui.base.BaseFragment
import com.vipulasri.trademetask.ui.listing.ListingsActivity
import com.vipulasri.trademetask.widgets.BottomSpaceItemDecoration
import kotlinx.android.synthetic.main.fragment_categories.*
import javax.inject.Inject

@FragmentScoped
class CategoriesFragment : BaseFragment(), CategoriesContract.View, CategoriesAdapter.Callbacks {

    interface Callback {
        fun onCategoryClick(category: Category)
    }

    @Inject
    lateinit var mPresenter: CategoriesPresenter

    private var mCategories = arrayListOf<Category>()
    private lateinit var mCategoryAdapter: CategoriesAdapter
    private var mCallback: Callback? = null

    fun newInstance(callback: Callback): CategoriesFragment {
        val categoriesFragment = CategoriesFragment()
        categoriesFragment.setCallback(callback)
        return categoriesFragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflateView(R.layout.fragment_categories, container!!, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.addItemDecoration(DividerItemDecoration(context, OrientationHelper.VERTICAL))

        mCategoryAdapter = CategoriesAdapter(mCategories)
        mCategoryAdapter.setCallbacks(this)
        recyclerView.adapter = mCategoryAdapter
    }

    override fun onResume() {
        super.onResume()
        mPresenter.takeView(this)
        mPresenter.getTopCategories()
    }

    override fun onPause() {
        super.onPause()
        mPresenter.dropView()
    }

    override fun showCategories(categories: List<Category>) {
        mCategories.clear()
        mCategories.addAll(categories)
        mCategoryAdapter.notifyDataSetChanged()
    }

    override fun onCategoryClick(category: Category) {
        mCallback?.onCategoryClick(category)
    }

    fun setCallback(callback: Callback) {
        mCallback = callback
    }
}