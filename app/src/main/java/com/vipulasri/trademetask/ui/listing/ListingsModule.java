package com.vipulasri.trademetask.ui.listing;

import com.vipulasri.trademetask.inject.FragmentScoped;
import com.vipulasri.trademetask.ui.categories.CategoriesContract;
import com.vipulasri.trademetask.ui.categories.CategoriesFragment;
import com.vipulasri.trademetask.ui.categories.CategoriesPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ListingsModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract ListingsFragment listingsFragment();

    @FragmentScoped
    @Binds
    abstract ListingsContract.Presenter listingsPresenter(ListingsPresenter presenter);
}
