package com.vipulasri.trademetask.ui.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import android.widget.ListPopupWindow
import com.jakewharton.rxbinding.widget.RxTextView
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.extensions.showSnackBar
import com.vipulasri.trademetask.inject.ActivityScoped
import com.vipulasri.trademetask.model.Category
import com.vipulasri.trademetask.ui.base.BaseActivity
import com.vipulasri.trademetask.ui.categories.CategoriesFragment
import com.vipulasri.trademetask.ui.listing.ListingsActivity
import com.vipulasri.trademetask.ui.listing.ListingsFragment
import com.vipulasri.trademetask.model.Listing
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_home.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject


@ActivityScoped
class HomeActivity : BaseActivity(), HomeContract.View {

    private var mTwoPane: Boolean = false

    @Inject
    lateinit var mPresenter: HomeContract.Presenter

    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        showCategoriesFragment()

        if (findViewById<View>(R.id.categoryListingFragment) != null) {
            mTwoPane = true;
        }

        initSearchBox()
    }

    private fun showCategoriesFragment() {
        replaceFragment(CategoriesFragment().newInstance(object : CategoriesFragment.Callback{
            override fun onCategoryClick(category: Category) {
                if(mTwoPane) {
                    replaceListingsFragment(ListingsFragment().newInstance(category))
                } else {
                    ListingsActivity().launchActivity(this@HomeActivity, category)
                }
            }

        }))
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.categoriesFragment, fragment)
                .commit()
    }

    private fun replaceListingsFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.categoryListingFragment, fragment)
                .commit()
    }

    override fun onResume() {
        super.onResume()
        mPresenter.takeView(this)
    }

    override fun onPause() {
        super.onPause()
        mPresenter.dropView()
        disposable.clear()
    }

    private fun initSearchBox() {

        val observable = RxTextView.textChanges(edit_search)
                .filter{ it.length>2 }
                .debounce(500, TimeUnit.MILLISECONDS)
                .map{ it.toString() }
                .observeOn(AndroidSchedulers.mainThread())

        disposable.add(observable
                            .subscribe({ mPresenter.search(it)
                    }, {
                        Log.e("error", "->" + it.message)
                    }))
    }

    override fun showSearchResults(listings: List<Listing>) {
        val listPopupWindow = ListPopupWindow(this)
        listPopupWindow.setAdapter(SearchAdapter(this, R.layout.layout_spinner_multi_row, listings))
        listPopupWindow.anchorView = edit_search
        listPopupWindow.isModal = true
        listPopupWindow.setOnItemClickListener { adapterView, view, position, id ->
            val listing = listings[position]
            showSnackBar(findViewById(android.R.id.content), listing.Title)
            listPopupWindow.dismiss()
        }
        edit_search.post {
            listPopupWindow.width = edit_search.measuredWidth
        }
        listPopupWindow.show()
    }

}
