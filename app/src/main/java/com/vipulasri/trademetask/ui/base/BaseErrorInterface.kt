package com.vipulasri.trademetask.ui.base

import android.view.View
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.data.remote.ErrorResponse
import com.vipulasri.trademetask.data.remote.ErrorUtils
import com.vipulasri.trademetask.extensions.getString
import com.vipulasri.trademetask.extensions.showSnackBar
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

interface BaseErrorInterface {

    fun showError(throwable: Throwable, view: View) {
        when (throwable) {
            is HttpException -> {
                val response = throwable.response()

                val errorResponse = ErrorUtils.parseError(response.errorBody()!!.string(), ErrorResponse::class.java)

                when(response.code()) {
                    //422 -> showValidationError(errorResponse.errors)
                    403 -> onForbiddenError(errorResponse, view)
                    404 -> onNotFound(errorResponse, view)
                    else -> showOtherError(errorResponse, view)
                }

            }
            is SocketTimeoutException -> showSocketTimeoutError(view)
            is IOException -> showInternetError(view)
        }
    }

    fun showInternetError(view: View) {
        showSnackBar(view, getString(view.context!!, R.string.error_msg_no_internet))
    }

    fun showSocketTimeoutError(view: View) {
        showSnackBar(view, getString(view.context!!, R.string.error_msg_timeout))
    }

    fun onForbiddenError(errorResponse: ErrorResponse<*>, view: View) {
        showOtherError(errorResponse, view)
    }

    fun onNotFound(errorResponse: ErrorResponse<*>, view: View) {
        showOtherError(errorResponse, view)
    }

    fun showOtherError(errorResponse: ErrorResponse<*>, view: View) {
        if (errorResponse.message.orEmpty().isNotEmpty())
            showSnackBar(view, errorResponse.message)
    }

}