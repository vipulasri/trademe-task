package com.vipulasri.trademetask.ui.categories

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.vipulasri.trademetask.R
import com.vipulasri.trademetask.extensions.inflateView
import com.vipulasri.trademetask.model.Category
import kotlinx.android.synthetic.main.item_category.view.*

class CategoriesAdapter(var mFeedList: MutableList<Category>) : RecyclerView.Adapter<CategoriesAdapter.ActivityViewHolder>() {

    interface Callbacks {
        fun onCategoryClick(category: Category)
    }

    private var context: Context? = null
    private var callbacks: Callbacks? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivityViewHolder {
        context = parent.context
        return ActivityViewHolder(inflateView(R.layout.item_category, parent, false))
    }

    override fun onBindViewHolder(holder: ActivityViewHolder, position: Int) {
        val category = mFeedList[position]
        holder.name.text = category.Name

        holder.itemView.setOnClickListener { callbacks?.onCategoryClick(category) }
    }

    override fun getItemCount(): Int {
        return mFeedList.size;
    }

    fun setCallbacks(callbacks: Callbacks) {
        this.callbacks = callbacks
    }

    class ActivityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.text_category_name
    }

}