package com.vipulasri.trademetask.ui.listing

import android.util.Log
import com.vipulasri.trademetask.data.ListingRepository
import com.vipulasri.trademetask.extensions.whenNotNull
import com.vipulasri.trademetask.inject.FragmentScoped
import javax.inject.Inject

import io.reactivex.disposables.CompositeDisposable

@FragmentScoped
class ListingsPresenter @Inject
internal constructor(private val mCategoryRepository: ListingRepository) : ListingsContract.Presenter {

    private var mView: ListingsContract.View? = null

    private val mDisposables: CompositeDisposable = CompositeDisposable()

    override fun getListingByCategory(category: String) {

        whenNotNull(mView) {
            it.showLoading(true)
        }

        val disposable = mCategoryRepository
                .getListingByCategory(category)
                .subscribe({ listings ->
                    mView?.showLoading(false)
                    mView?.showListings(listings)
                }, { throwable ->
                    Log.e("error", "->" + throwable.message)
                    mView?.showLoading(false)
                    mView?.showError(throwable)
                })

        mDisposables.add(disposable)
    }

    override fun takeView(view: ListingsContract.View) {
        mView = view
    }

    override fun dropView() {
        mView = null
        mDisposables.clear()
    }

    companion object {

        private val TAG = ListingsContract::class.java.simpleName
    }
}
