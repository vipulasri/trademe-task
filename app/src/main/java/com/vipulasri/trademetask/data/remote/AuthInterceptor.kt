package com.vipulasri.trademetask.data.remote

import com.vipulasri.trademetask.BuildConfig

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AuthInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        var request = chain.request()
        val requestBuilder = request.newBuilder()

        requestBuilder.addHeader("Authorization", "OAuth oauth_consumer_key=${BuildConfig.KEY}, oauth_signature_method=\"PLAINTEXT\", oauth_signature=${BuildConfig.SECREt}&")

        request = requestBuilder.build()

        return chain.proceed(request)
    }
}