package com.vipulasri.trademetask.data.remote;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

public class ErrorUtils {

    public static <T> T parseError(@NonNull String json, Class<T> responseClass) {
        return new Gson().fromJson(json, responseClass);
    }

}
