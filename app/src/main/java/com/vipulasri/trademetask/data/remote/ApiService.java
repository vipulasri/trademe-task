package com.vipulasri.trademetask.data.remote;

import com.vipulasri.trademetask.dto.CategoryResponse;
import com.vipulasri.trademetask.dto.ListingResponse;
import com.vipulasri.trademetask.model.Category;
import com.vipulasri.trademetask.model.Listing;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("Categories/0.json")
    Observable<CategoryResponse> getTopCategories(@Query("depth") int depth);

    @GET("Search/General.json")
    Observable<ListingResponse> getCategoryListing(@Query("category") String category,
                                                   @Query("rows") int rows,
                                                   @Query("photo_size") String photoSize);

    @GET("Listings/{listing_id}.json")
    Observable<Listing> getListingDetails(@Path("listing_id") String listingId);

    @GET("Search/General.json")
    Observable<ListingResponse> search(@Query("search_string") String search);
}
