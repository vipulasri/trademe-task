package com.vipulasri.trademetask.data

import com.vipulasri.trademetask.data.remote.ApiService
import com.vipulasri.trademetask.model.Category
import com.vipulasri.trademetask.model.Listing
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListingRepository @Inject
constructor(private val mApiService: ApiService){

    fun getListingByCategory(category: String): Observable<List<Listing>> {
        return mApiService.getCategoryListing(category, 20, "Large")
                .map { it.list }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getListingDetails(listingId: String): Observable<Listing> {
        return mApiService.getListingDetails(listingId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
    }

}
