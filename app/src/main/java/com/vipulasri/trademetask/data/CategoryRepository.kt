package com.vipulasri.trademetask.data

import com.vipulasri.trademetask.data.remote.ApiService
import com.vipulasri.trademetask.model.Category
import com.vipulasri.trademetask.model.Listing
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CategoryRepository @Inject
constructor(private val mApiService: ApiService){

    private var mCachedCategories = mutableListOf<Category>()

    fun getTopCategories(): Observable<List<Category>> {

        if(mCachedCategories.isNotEmpty())
            return Observable.fromArray(mCachedCategories)

        return mApiService.getTopCategories(1)
                .map { it.subcategories }
                .doOnNext { categories->
                    mCachedCategories.addAll(categories)
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun search(query: String): Observable<List<Listing>> {
        return mApiService.search(query)
                .map { it.list }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
    }

}
