package com.vipulasri.trademetask.data.remote;

import com.google.gson.annotations.SerializedName;

public class ErrorResponse<T> {

    @SerializedName("ErrorDescription") private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

