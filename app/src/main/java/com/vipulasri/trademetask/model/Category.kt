package com.vipulasri.trademetask.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(
		val Name: String,
		val Number: String,
		val Path: String
) : Parcelable