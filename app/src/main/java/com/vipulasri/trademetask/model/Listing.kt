package com.vipulasri.trademetask.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Listing(
        val ListingId: Int,
        val Title: String,
        val Category: String,
        val StartPrice: String,
        val StartDate: String,
        val EndDate: String,
        val IsFeatured: Boolean,
        val HasGallery: Boolean,
        val AsAt: String,
        val CategoryPath: String,
        val PictureHref: String,
        val IsNew: Boolean,
        val Region: String,
        val Suburb: String,
        val Body: String? = null
): Parcelable