package com.vipulasri.trademetask

import android.os.StrictMode
import com.vipulasri.trademetask.inject.DaggerAppComponent
import com.vipulasri.trademetask.inject.NetworkModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class TaskApplication : DaggerApplication() {

    val TAG = TaskApplication::class.java.simpleName

    override fun onCreate() {
        super.onCreate()

        instance = this

        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .permitDiskReads()
                    .penaltyLog()
                    .build())
            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build())
        }

    }

    companion object {
        lateinit var instance: TaskApplication
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder()
                .application(this)
                .networkModule(NetworkModule(BuildConfig.API_BASE_URL))
                .build()
        appComponent.inject(this)
        return appComponent
    }
}
