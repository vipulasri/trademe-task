package com.vipulasri.trademetask.extensions

import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

fun ImageView.loadImage(image: Any) {
    Glide.with(this)
            .load(image)
            .apply(RequestOptions.centerCropTransform().priority(Priority.HIGH))
            .into(this)
}

fun ImageView.loadImageUrl(imageUrl: String) {
    loadImageUrl(imageUrl, RequestOptions.centerCropTransform())
}

fun ImageView.loadImageUrl(imageUrl: String, requestOptions: RequestOptions) {
    Glide.with(this)
            .load(imageUrl)
            .apply(requestOptions.priority(Priority.HIGH))
            .into(this)
}