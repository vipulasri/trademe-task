package com.vipulasri.trademetask.extensions

import android.content.Context
import android.content.res.Resources
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun dpToPx(dp: Float, context: Context): Int {
    return dpToPx(dp, context.resources)
}

fun dpToPx(dp: Float, resources: Resources): Int {
    val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)
    return px.toInt()
}

fun inflateView(@LayoutRes layoutResId: Int, parent: ViewGroup, attachToRoot: Boolean): View {
    return LayoutInflater.from(parent.context).inflate(layoutResId, parent, attachToRoot)
}

inline fun <T:Any, R> whenNotNull(input: T?, callback: (T)->R): R? {
    return input?.let(callback)
}

infix fun <T> Boolean.then(param: T): T? = if(this) param else null

fun getSnackBar(view: View, value: String): Snackbar = getSnackBar(view, value, Snackbar.LENGTH_LONG)

fun getSnackBar(view: View, value: String, length: Int): Snackbar {
    return Snackbar.make(view, value, length)
}

fun showSnackBar(view: View, value: String) {
    getSnackBar(view, value).show()
}

fun showSnackBar(view: View, value: Int) {
    getSnackBar(view, view.context.getString(value)).show()
}

fun showSnackBar(view: View, value: Int, length: Int) {
    getSnackBar(view, view.context.getString(value), length).show()
}

fun getString(context: Context?, stringRes: Int) : String = context!!.resources.getString(stringRes)