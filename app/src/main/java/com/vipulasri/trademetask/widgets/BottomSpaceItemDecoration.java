package com.vipulasri.trademetask.widgets;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class BottomSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int spacing;

    public BottomSpaceItemDecoration(int spacing) {
        this.spacing = spacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(0, 0, 0, spacing);
    }
}
