
package com.vipulasri.trademetask.inject

import android.content.Context
import com.vipulasri.trademetask.TaskApplication
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideContext(application: TaskApplication): Context {
        return application.applicationContext
    }
}