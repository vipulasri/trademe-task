package com.vipulasri.trademetask.inject;

import com.vipulasri.trademetask.ui.home.HomeModule;
import com.vipulasri.trademetask.ui.home.HomeActivity;
import com.vipulasri.trademetask.ui.listing.ListingsActivity;
import com.vipulasri.trademetask.ui.listing.ListingsModule;
import com.vipulasri.trademetask.ui.listingDetails.ListingDetailsActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = HomeModule.class)
    abstract HomeActivity homeActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ListingsModule.class)
    abstract ListingsActivity listingsActivity();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract ListingDetailsActivity listingDetailsActivity();

}
