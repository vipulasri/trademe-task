
package com.vipulasri.trademetask.inject;

import android.app.Application
import com.vipulasri.trademetask.TaskApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton
import dagger.BindsInstance

@Singleton
@Component(modules = arrayOf(
        AndroidSupportInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        ActivityBindingModule::class))
interface AppComponent : AndroidInjector<TaskApplication> {

    override fun inject(instance: TaskApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent

        fun networkModule(networkModule: NetworkModule): Builder
    }
}